    section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    xor rdi, rdi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax    ;обнуляем счетчик
    .cycle:
        cmp byte[rdi+rax], 0
        je .exit
        inc rax
        jmp .cycle
    .exit: ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length			
	   mov rsi, rdi				
	   mov rdx, rax				
	   mov rax, 1					
	   mov rdi, 1					
	   syscall
	   xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi    ;сохраняем символ в стеке
    mov rsi, rsp    ;адрес вершины стека
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    syscall
    pop rdi     ;забираем символ из стека
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r10
    push r11
    mov r10, rsp ; сохраним вершину стека
    mov r11, 10 ; сохраним основание системы счисления для деления
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
	.loop:
		dec rsp ; сдвигаем указатель
		xor rdx, rdx
		div  r11 ; делим на 10, остаток сохраняется в rdx
		add rdx, 0x30 ; переводим в ASCII
		mov  byte[rsp], dl ; сохраняем в стeк
		test rax, rax 
		jz .print 
		jmp .loop
	.print:
	 mov rdi, rsp
	 call print_string 
	 mov rsp, r10						
 pop r11
 pop r10 ; восстановим регистры
 ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jl .do_sign  ; если число отрицательное, переходим к .do_sign
    jmp print_uint
    .do_sign:
        push rdi
        mov rdi, '-'    ; заносим - в rdi
        call print_char     ; выводим -
        pop rdi         ; достаем число
        neg rdi         ; приводим к положительному
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov r10b,byte [rdi]
        mov r11b,byte [rsi]
        inc rdi
        inc rsi
        cmp r10b, r11b
        jne .n_equal
        cmp r10b, 0
        je .equal
        jmp .loop
    .equal:
        mov rax, 1
        ret
    .n_equal:
        mov rax, 0
        ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rax, 0
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 0
    syscall
    cmp rax, 0
    jle .aaa
    pop rax
    ret
    .aaa:
        pop rax
        mov rax, 0
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r15
    push rbx        ; сохраняем счетчик длины
    mov r12, rdi    ; сохраняем адрес буфера
    mov r13, rsi    ; и размер буфера
    mov r15, rdi
    mov rbx, 0      ; обнуляем счетчик длины
    dec r13

    .loop:
        mov rax, 0
        cmp r13, 0
        jle .return
        call read_char
        push rax    
        cmp rax, 0
        jnz .rax_not_0
        cmp r12, r15
        jz .return_rax
        jmp .is_space
        .rax_not_0:
            jz .is_space
            mov rdi, rax
            call space
            cmp rax, 0
            jz .no_space
        .is_space:
            pop rax
            cmp r15, r12
            je .loop
            mov [r12], byte 0
            mov rax, r15
            mov rdx, rbx
            jmp .return
        .no_space:
            pop rax
            mov byte [r12], al
            inc rbx 
            inc r12
            dec r13
            jmp .loop
    .return_rax:
        pop rax
        xor rax, rax
        xor rdx, rdx
    .return:
        pop rbx
        pop r15
        pop r13
        pop r12 
        ret

space:
    cmp rdi, 0x20
    je .ret
    cmp rdi, 0x9
    je .ret
    cmp rdi, 0xA
    je .ret
    mov rax, 0
    ret
    .ret:   
        mov rax, 1
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rsi, rsi
    mov r8, 10
    xor rcx, rcx
    xor rdx, rdx
	.loop:
    	mov sil, [rdi+rcx] 
	    cmp sil, 0x30 ; проверяем, что этот символ цифра
    	jl .return
    	cmp sil, 0x39   ; символ>=1 и символ<=9
    	jg .return
    	inc rcx
    	sub sil, 0x30 ; переводим символ в число
    	mul r8      ; умножаем на 10, для наращивания числа
    	add rax, rsi
    	jmp .loop
	.return:
    mov rdx, rcx    ; возвращаем длину числа
    ret
  
    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], 0x2d    ; проверяем, есть ли знак "-"
    je .parse_sign
    call parse_uint
    ret
.parse_sign:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .return
    neg rax
    inc rdx
.return:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi -> rsi -> rdx
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx    ; сравниваем длину строки с размером буфера
    jnl .loop       
    mov rdx, rax
    inc rdx
    mov r11, 0
    .loop:
        cmp r11, rdx
        jnl .loop_exit
        mov r10b, byte [rdi + r11]
        mov byte [rsi + r11], r10b
        inc r11
        jmp .loop
    .loop_exit:
        mov r11, rax
        inc r11
        cmp r11, rdx
        jg .return_0
        ret
    .return_0:
        mov rax, 0
        ret